package gormzap

import (
	"go.uber.org/zap"
)

// New create logger object for *gorm.DB from *zap.Logger
func New(zap *zap.Logger, opt ...bool) *Logger {
	asDebug := false
	if len(opt) > 0 {
		asDebug = opt[0]
	}
	return &Logger{
		zap:     zap,
		asDebug: asDebug,
	}
}

// Logger is an alternative implementation of *gorm.Logger
type Logger struct {
	zap     *zap.Logger
	asDebug bool
}

// Print passes arguments to Println
func (l *Logger) Print(values ...interface{}) {
	l.Println(values)
}

// Println format & print log
func (l *Logger) Println(values []interface{}) {
	if l.asDebug {
		l.zap.Debug("gorm", createLog(values).toZapFields()...)
	} else {
		l.zap.Info("gorm", createLog(values).toZapFields()...)
	}
}
